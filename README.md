# CircuitPython Stage Library Sprite Bank Creator
# Created by Vladislav Stadnik for himself to make CircuitPython Game developing little more faster and comfortable 
#April, 2020.

This is simple quickly written tkinter tool, simplified for creating spritesheet ([Bank](https://circuitpython-stage.readthedocs.io/en/latest/README.html#stage.Bank)) for Circuitpython's  [Stage game library](https://circuitpython-stage.readthedocs.io/en/latest/README.html)


It automatically makes the image with correct resolution (16x256), correct background color (which recognized by Stage as transparent layer) and resizing all the chosen images to 16x16 pixels.

There are 3 modes:
- create separate sprites 16x16 for further editing (turn in "Separate sprites" checkbox)
- create Bank from choosen images in current window (turn on "SpriteSheet" checkbox only)
- create sprites from previously saved frames (turn on both "SpriteSheet" and "From saved sprites" checkboxes)

After launching and saving sprite the "data" folder will be created. It includes "separate" and "full" folders. "Separate" folder has all the frames (please don't change names, especially before creating full image from them). "Full" folder has final Bank, but in PNG.

Unfortunately, PNG is not supported by Stage library, so you need to convert it to bmp (you can use Paint even, just click "Save as" bmp format, don't touch any more settings).

For now, I didn't find the way to save 4 bit-color bmp file, PIL can`t do it.



