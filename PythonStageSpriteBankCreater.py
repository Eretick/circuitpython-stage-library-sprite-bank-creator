import tkinter
from tkinter import Tk, Button, IntVar, Label, messagebox
from tkinter import ttk
from tkinter.filedialog import askopenfilename

import PIL.ImageColor
from PIL import Image, ImageTk
from os import path, getcwd, mkdir, listdir
from natsort import natsorted
import png #pip install pypng
import numpy as np

CANVAS_IMAGE_WIDTH = 32
CANVAS_IMAGE_HEIGHT = 32
SPRITE_BLOCK_SIZE = 16
SPRITE_BLOCK_COUNT = 16
FINAL_IMAGE_WIDTH = 16
FINAL_IMAGE_HEIGHT = 256
COLORS = {'black': 'black', 'purple': 'purple', 'stage':'#ff00ff'}
BACKGROUND_COLOR = COLORS['stage']
BACKGROUND_COLOR_RGB = PIL.ImageColor.getrgb(BACKGROUND_COLOR)
DATA_PATH = path.join(getcwd(),'data')
FULL_IMG_DIR = path.join(DATA_PATH, 'full')
FULL_IMG_PATH = path.join(FULL_IMG_DIR, 'full.png')
SEP_IMG_DIR = path.join(DATA_PATH, 'separate')
PALETTE_MODE = 'RGB' # default 'RGB'
PALETTE = (
    (0x00, 0x00, 0x00),
    (0x00, 0xbb, 0x11),
    (0xaa, 0x22, 0x00),
    (0xff, 0xaa, 0x00),
)


def create_empty_image(size: list = (16, 16), color: str = BACKGROUND_COLOR):
    #I - 16 bit color palette
    return Image.new(PALETTE_MODE, size, color)#.convert(mode='P', colors=16)

    # proportional scaling function
def get_scaled_size(width = 16, height=16, final_width=CANVAS_IMAGE_WIDTH, final_height=CANVAS_IMAGE_HEIGHT):
    if width > height:
        scaler = int(width / final_width)
        new_width = int(width / scaler)
        new_height = int(height / scaler)
    elif width < height:
        scaler = height / final_height
        new_width = width / scaler
        new_height = height / scaler
    else:
        new_width = final_width
        new_height = final_height
    return int(new_width), int(new_height)

class SpritePlace(ttk.Frame):

    def __init__(self, parent=None, text=None):
        ttk.Frame.__init__(self, parent)
        ttk.Label(self, text=text).pack(padx=10, pady=(10,0))
        # blank black image at start of program
        self.image = create_empty_image(size=(CANVAS_IMAGE_WIDTH, CANVAS_IMAGE_HEIGHT), color=BACKGROUND_COLOR)
        self.photo = ImageTk.PhotoImage(self.image)
        self.sprite_label = ttk.Label(self, relief='sunken', image=self.photo)
        self.sprite_label.image = self.image
        self.sprite_label.pack(padx=10, pady=(0,10))
        self.image_btn = ttk.Button(self, text="Import", command=self.load_image)
        self.image_btn.pack(padx=10, pady=(0,10))
        self["relief"] = 'groove'

        # loaded image path
        self.path = ''


    def load_image(self):
        filename = askopenfilename(filetypes=[("Images", ".jpg .jpeg .png .gif .png")])
        if filename:
            self.path = filename
            self.change_image(self.path)

    def change_image(self, image):
        if type(image) == str:
            scaler = 1
            self.image = Image.open(image)
            # scale down picture with saving the proportions
            size = get_scaled_size(width=self.image.width, height=self.image.height)
            self.image = self.image.resize(size, Image.LANCZOS)
        self.photo = ImageTk.PhotoImage(self.image)
        self.sprite_label["image"] = self.photo
        self.sprite_label.image = self.photo

class Main(Tk):
    def __init__(self):
        Tk.__init__(self)
        self.title("Python Stage Sprite Bank Creator")
        # main frame
        self.main_frame = ttk.Frame(self)
        self.main_frame.grid(row=0, column=0)

        #Style
        style = ttk.Style()
        style.theme_use('alt')
        style.configure('TButton', background = 'red', foreground = 'white', width = 10, borderwidth=1, focusthickness=3, focuscolor='none')
        style.map('TButton', background=[('active','red')])
        print(style.theme_names())
        

        # All sprite frames list for easy for-loop logic
        self.sprite_frames = []
        
        frame_counter = 1
        for r in range(2):
            for c in range(8):
                sprite_frame = SpritePlace(self.main_frame, text=f"Frame {frame_counter}")
                sprite_frame.grid(row=r, column=c, padx=10, pady=10)
                self.sprite_frames.append(sprite_frame)
                frame_counter += 1

        # Saving interface part
        ttk.Label(self.main_frame, text="Save mode:").grid(row=2, column=3, columnspan=2)

        # Separate sprite blocks checkbutton
        self.sep_group = IntVar()
        self.separate_checkbox = ttk.Checkbutton(self.main_frame, text="Separate sprites",
                                                 variable=self.sep_group, onvalue=1, offvalue=0)
        self.separate_checkbox.grid(row=3, column=3, columnspan=2, sticky="w")
        # full sprite Bank with the pictures in the window
        self.full_group = IntVar()
        self.full_checkbox = ttk.Checkbutton(self.main_frame, text="Sprite Sheet", variable=self.full_group,
                                             onvalue=1, offvalue=0, command=self.change_full_state)
        self.full_checkbox.grid(row=4, column=3, sticky="w")
        # full sprite Bank with the separate sprites created earlier by using separate checkbutton
        self.full_prepared_group = IntVar()
        self.full_prepared_checkbox = ttk.Checkbutton(self.main_frame, text="From saved sprites",
                                             variable=self.full_prepared_group, onvalue=1, offvalue=0, state='disabled')
        self.full_prepared_checkbox.grid(row=4, column=4)

        # bottom buttons
        self.create_btn = ttk.Button(self.main_frame, text="Create", command=self.create_image)
        self.create_btn.grid(row=5, column=3, columnspan=1, padx=10, pady=10)
        self.clear_btn = ttk.Button(self.main_frame, text="Clear", command=self.clear_labels)
        self.clear_btn.grid(row=5, column=4, columnspan=1, padx=10, pady=10)

        self.check_dirs()

    # remove all loaded images and paste blank image with default bg color
    def clear_labels(self):
        for frame in self.sprite_frames:
            frame.path = ''
            frame.image = create_empty_image(size=(CANVAS_IMAGE_WIDTH, CANVAS_IMAGE_HEIGHT), color=BACKGROUND_COLOR)
            frame.photo = ImageTk.PhotoImage(frame.image)
            frame.sprite_label.image = frame.photo
            frame.sprite_label["image"] = frame.photo


    # controlling the state of "full-from-separate" checkbutton
    def change_full_state(self):
        # Включаем/отключаем возможность создания Bank из заранее созданных кадров в папке SEP_IMG_DIR
        if not 'disabled' in self.full_prepared_checkbox.state():
            self.full_prepared_checkbox.state(['disabled'])
        else:
            self.full_prepared_checkbox.state(['!disabled'])

    # creating full Bank
    def save_full_img(self):
        dst = Image.new(PALETTE_MODE, (FINAL_IMAGE_WIDTH, SPRITE_BLOCK_SIZE*SPRITE_BLOCK_COUNT), color=BACKGROUND_COLOR)
        # offset for every new block of image
        offset_y = 0
        offset_x = 0
        # create the Bank from separate saved blocks only if appropriate checkbutton is active
        if not 'disabled' in self.full_prepared_checkbox.state() and 'selected' in self.full_prepared_checkbox.state():
            files = listdir(SEP_IMG_DIR)
            files = natsorted(files)
            for i in range(len(files)):
                # open original saved sprite
                new_path = path.join(SEP_IMG_DIR, files[i])
                i_im = Image.open(new_path)#.convert(mode='P', colors=16)
                # convart to RGBA for access to transparent pixels
                i_im = self.paste_clear_img(i_im)
                offset_x = (SPRITE_BLOCK_SIZE - i_im.size[0]) // 2
                offset_y = SPRITE_BLOCK_SIZE*i + (SPRITE_BLOCK_SIZE - i_im.size[1]) // 2
                dst.paste(i_im, (offset_x, offset_y))
        # create the Bank from images in window
        else:
            frames = self.sprite_frames
            for frame in range(len(frames)):
                new_path = frames[frame].path
                if new_path != '':
                    i_im = Image.open(new_path).resize(get_scaled_size(
                        width=frames[frame].image.width, height=frames[frame].image.height,
                        final_width=SPRITE_BLOCK_SIZE, final_height=SPRITE_BLOCK_SIZE), Image.LANCZOS)
                else:
                    i_im = create_empty_image(color=BACKGROUND_COLOR).convert(mode='P', colors=16)

                i_im = self.paste_clear_img(i_im)
                offset_x = (SPRITE_BLOCK_SIZE - i_im.size[0]) // 2
                offset_y = SPRITE_BLOCK_SIZE*frame + (SPRITE_BLOCK_SIZE - i_im.size[1]) // 2
                dst.paste(i_im, (offset_x, offset_y))
        # creating PNG image with 4 bit color
        self.convert_to_PNG(dst, FULL_IMG_PATH)
        # converting to gif

        #dst.save(FULL_IMG_PATH, format='BMP')

    def paste_clear_img(self, image):
        i_im = image.convert('RGBA')
        # pixel`s data list of new not transparent image
        newImage = []
        # making data of non-transparent image
        for item in i_im.getdata():
            # replacing transparent pixels by background color
            if item == (0, 0, 0, 0):
                newImage.append((BACKGROUND_COLOR_RGB[0], BACKGROUND_COLOR_RGB[1], BACKGROUND_COLOR_RGB[2], 255))
            else:
                newImage.append(item)
        # replacing transparent image by non-transparent copy
        i_im.putdata(newImage)
        # saving image with python Stage library format
        i_im = i_im.convert(mode='P', colors=16)
        return i_im

    def convert_to_PNG(self, image: Image, path: str):
        """
        Function creates PNG with 4 bit color mode, thanks to
        https://stackoverflow.com/questions/62765455/convert-images-bit-depth-with-pypng
        :param image: Image object from PIL
        :return: None
        """
        # convert RGB to 16 colors
        image = image.quantize(16)

        # get palette as flat list [r, g, b, r, g, b, ...]
        palette = image.getpalette()
        # conver flat list to tupled [(r, g, b), (r, g, b), ...]
        palette = [tuple(palette[x:x + 3]) for x in range(0, len(palette), 3)]
        palette = palette[:16]

        # get pixels/indexes as numpy array
        im = np.array(image)

        with open(path, 'wb') as f:
            # png_writer = png.Writer(im.shape[1], im.shape[0], bitdepth=4)  # without palette
            png_writer = png.Writer(im.shape[1], im.shape[0], bitdepth=4, palette=palette)  # with palette
            png_writer.write(f, im)


    def save_frames(self):
        for index in range(len(self.sprite_frames)):
            cur_frame = self.sprite_frames[index]
            cur_image = cur_frame.image
            if type(cur_image) == Image.Image:
                size = get_scaled_size(width=cur_image.width, height=cur_image.height,
                    final_width=SPRITE_BLOCK_SIZE, final_height=SPRITE_BLOCK_SIZE)
                cur_image = cur_image.resize(size)
                cur_image.convert(mode='P', colors=16)
                cur_image.save(path.join(SEP_IMG_DIR, f"index0{index+1}.png"))
            else:
                create_empty_image(color=BACKGROUND_COLOR,
                                   size=[16, 16]).save(path.join(SEP_IMG_DIR, f"index{index+1}.png"))

    def check_dirs(self):
        # creating all dirs
        if not path.exists(DATA_PATH):
            mkdir(DATA_PATH)

        if not path.exists(FULL_IMG_DIR):
            mkdir(FULL_IMG_DIR)
        if not path.exists(SEP_IMG_DIR):
            mkdir(SEP_IMG_DIR)

    def create_image(self):

        # saving the image(s)
        if self.sep_group.get() == 1:
            self.save_frames()
        if self.full_group.get() == 1:
            self.save_full_img()
        if self.sep_group.get() == 0 and self.full_group.get() == 0:
            messagebox.showinfo('', 'Please choose saving mode!')
        
if __name__ == "__main__":
    win = Main()
    win.mainloop()
